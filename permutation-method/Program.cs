﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LR2
{
    class Program
    {
        static void Main(string[] args)
        {           
            string text = getTextFile("text.txt");            
            string newText = "";
            string textOrigin = "";

            newText = Encryption(text, false);
            Console.WriteLine(newText);
            textOrigin = Encryption(newText, true);
            Console.WriteLine(textOrigin);
            
            Console.ReadLine();
            Console.WriteLine("Good Bye!");
            System.Threading.Thread.Sleep(100);
            Environment.Exit(0);  
        }

        static public string Encryption(string text, bool decrypt)
        {
            text = text.Trim();
            string newText = "";
            if (false == decrypt)
            {
                newText = EncryptText(text);
            }
            else
            {
                newText = DecryptText(text);
            }
            return newText;
        }

        static public string EncryptText(string text)
        {
            string[] alphabet = { "йцукенгш", "щзхїєждл", "орпавіфя", "чсмитьбю", "., ЦУКЕН", "ГШЩЗХФІВ", "АПРОЛДЖЄ", "ЯЧСМИТБЮ" };
            string[] alphabetNew = { "А", "Б", "В", "Г", "Д", "Ж", "О", "У" };
            //string alphabetNew2 =  "АБВГДЖОУ";
            string textNew = "";
            for (int j = 0; j < text.Length; j++)
                {
                    for (int i = 0; i < alphabet.Length; i++)
                    {
                        int alphabetIndex = alphabet[i].IndexOf(text[j]);
                        if (alphabetIndex != -1)
                        {
                            textNew += alphabetNew[i] + alphabetNew[alphabetIndex];
                            break;
                        }

                    }
                }
            return textNew;
        }

        static public string DecryptText(string text)
        {
            string[] alphabet = { "йцукенгш", "щзхїєждл", "орпавіфя", "чсмитьбю", "., ЦУКЕН", "ГШЩЗХФІВ", "АПРОЛДЖЄ", "ЯЧСМИТБЮ" };
            string[] alphabetNew = { "А", "Б", "В", "Г", "Д", "Ж", "О", "У" };            
            string letterNew = "";
            for (int i = 0; i < text.Length; i = i + 2)
            {
                int alphabetNewIndexOne = string.Join("", alphabetNew).IndexOf(text[i]);
                int alphabetNewIndexTwo = string.Join("", alphabetNew).IndexOf(text[i + 1]);
                letterNew += alphabet[alphabetNewIndexOne][alphabetNewIndexTwo].ToString();
            }
            return letterNew;
        }

        static public string getTextFile(string pathFile)
        {
            StreamReader readerText = new StreamReader(pathFile, Encoding.Default);
            string text = readerText.ReadToEnd();
            readerText.Close();
            return text;
        }   

        static public void saveToFile(string pathFile, string text)
        {
            StreamWriter writerKey = new StreamWriter(pathFile, false, Encoding.Default);
            writerKey.WriteLine(text);
            writerKey.Close();
        }
    }
}
