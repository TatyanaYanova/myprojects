﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LR1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введіть і'мя файлу з текстом");
            string pathFile = Console.ReadLine();            
            string text = getTextFile(pathFile);            
            int[] encryptionKey;            
            string newText = "";
           
            Console.WriteLine("1 - шифрувати текст");
            Console.WriteLine("2 - розшиврувати текст");
            Console.WriteLine("0 - вихід");
            int indexEvent = int.Parse(Console.ReadLine());
            switch (indexEvent)
            {
                case 1:
                    encryptionKey = GenerateKey(text.Length);
                    newText = Encryption(text, encryptionKey, false);
                    saveToFile("new-text.txt", newText);                    
                    break;
                case 2:
                    Console.WriteLine("Введіть і'мя файлу з ключем");
                    string pathFileKey = Console.ReadLine();                    
                    string keyString = getTextFile(pathFileKey);
                    encryptionKey = GetKeyArray(keyString);                    
                    newText = Encryption(text, encryptionKey, true);
                    break;
                case 0:
                    Console.WriteLine("Good Bye!");
                    System.Threading.Thread.Sleep(100);
                    Environment.Exit(0);
                    break;
            }
            Console.WriteLine(newText);            
            Console.ReadLine();
        }

        static public int[] GenerateKey(int textLength)
        {
            int[] generateKey = new int[textLength];//массив случайных значений по длине текста
            string key = "";
            Random randNum = new Random();
            for (int i = 0; i < textLength; i++)
            {
                generateKey[i] = randNum.Next(1, 20);
                if (i == textLength - 1)
                {
                    key += Convert.ToString(generateKey[i]);                    
                }
                else
                {
                    key += Convert.ToString(generateKey[i]) + "-";                    
                }
            }
            Console.WriteLine(key);
            saveToFile("key.txt", key);
            return generateKey;
        }

        static public void saveToFile(string pathFile, string text)
        {
            StreamWriter writerKey = new StreamWriter(pathFile, false, Encoding.Default);
            writerKey.WriteLine(text);
            writerKey.Close();
        }

        static public string getTextFile(string pathFile)
        {
            StreamReader readerText = new StreamReader(pathFile, Encoding.Default);
            string text = readerText.ReadToEnd();
            readerText.Close();
            return text;
        }

        static public int[] GetKeyArray(string key)
        {
            int[] keyDecoder = key.Split('-').Select(Int32.Parse).ToArray();            
            return keyDecoder;
        }

        static public string Encryption(string text, int[] key, bool decrypt)
        {
            text = text.Trim();
            string alphabet = "йцукенгшщзхїєждлорпавіфячсмитьбю., ЙЦУКЕНГШЩЗХЇФІВАПРОЛДЖЄЯЧСМИТЬБЮ";
            string newText = "";
            int newIndex;
            for (int i = 0; i < text.Length; i++)
            {
                int alphabetIndex = alphabet.IndexOf(text[i]);
                if (false == decrypt)
                {
                    newIndex = ((alphabetIndex + key[i])) % alphabet.Length;
                }
                else
                {
                    newIndex = (alphabetIndex + alphabet.Length - key[i]) % alphabet.Length;
                }
                newText += alphabet[newIndex];
            }            
            return newText;
        }
        
    }
}
